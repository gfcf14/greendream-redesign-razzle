const paths = require('./paths');

module.exports = {
  // for custom webpack config
  modify(config, { target, dev }, webpack) {
    const appConfig = config;

    // uncomment this and do yarn start to output webpack rules
    // for (const rule of appConfig.module.rules) {
    //   console.log('============================================');
    //   console.log(`RULE: ${rule.test}`);
    //   console.log(rule);
    //   console.log('USE:');
    //   console.log(rule.use);
    //   console.log('============================================');
    // }

    appConfig.resolve['alias'] = {
      'components': paths.appComponents,
      'images': paths.appImages,
      'styles': paths.appStyles,
    };

    return appConfig;
  },
  plugins: [
    {
      name: 'scss',
      options: {
        postcss: {
          dev: {
            sourceMap: false,
          },
        },
      },
    }
  ],
};

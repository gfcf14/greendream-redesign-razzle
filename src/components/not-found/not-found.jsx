import React from 'react';
import { Flex, Image } from 'rebass';
import { vortexImage } from 'images';
import './not-found.scss';

export function NotFound() {
  return (
    <Flex as="section" className="not-found-rct-component">
      <section className="not-found-rct-component__text-section">
        <section>
          <div className="stars-sm" />
          <div className="stars-md" />
          <div className="stars-lg" />
        </section>
        <p className="center-text">
          404 page not found
        </p>
        <p className="bottom-text">
          Don't worry! Click here to go back to the homepage
        </p>
      </section>
      <section className="not-found-rct-component__rotated-section">
        <Image
          src={vortexImage}
          className="vortex"
          alt="vortex"
        />
      </section>
    </Flex>
  );
}


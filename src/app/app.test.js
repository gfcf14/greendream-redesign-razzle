import React from 'react';
import App from './app';
import MemoryRouter from 'react-router-dom/MemoryRouter';

describe('App Component Unit Test', () => {
  const wrapper = mountWithIntl(
    <MemoryRouter>
      <App />
    </MemoryRouter>
  );

  it ('renders without crashes', () => {
    expect(wrapper.exists()).toBe(true);
  });
});


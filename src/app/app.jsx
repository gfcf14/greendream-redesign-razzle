import React from 'react';
import Route from 'react-router-dom/Route';
import Switch from 'react-router-dom/Switch';
import { NotFound } from 'components';
import './app.scss';

const App = () => (
  <Switch>
    <Route exact path="/" component={NotFound} />
  </Switch>
);

export default App;

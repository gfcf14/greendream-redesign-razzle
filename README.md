# GreenDream Redesign
This project is a redesign for the website https://www.greendreampg.com/

# Motivation
I have tried to keep my website up to date with code practices, but my limited knowledge then
would only allow me to develop it with jQuery and plain CSS. This project serves as a refactoring
and enhancement of the existing code to use the latest coding practices with React, SASS and ES6

# Tech used
Built with:
- Razzle
- Jest (Enzyme)

# Installation
- Clone this repository
- Run `yarn install`. This will create a `node_modules` folder and install necessary dependencies

# Running
- Run `yarn start`. Then, open a browser window and navigate to `http://localhost:3000`

# Testing
- For Unit Testing, run `yarn test`

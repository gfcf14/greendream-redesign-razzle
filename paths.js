const path = require('path');

const resolveApp = relativePath => path.resolve(relativePath);

module.exports = {
  appComponents: resolveApp('src/components'),
  appImages: resolveApp('src/images'),
  appStyles: resolveApp('src/styles'),
};

